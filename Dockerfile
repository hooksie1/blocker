FROM golang:alpine
WORKDIR /app
RUN apk update && apk upgrade && apk add --no-cache ca-certificates gcc musl-dev libpcap-dev
RUN update-ca-certificates
ADD . /app/
RUN GOOS=linux go build -a -ldflags="-s -w" -installsuffix cgo -o blocker .

ENTRYPOINT ["./blocker"]

