package pkg

import (
	"github.com/google/gopacket"
	"github.com/google/gopacket/layers"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/spf13/viper"
	"log"
	"net"
	"testing"
)

type TestPackets struct {
	Packets []gopacket.Packet
}

type TestFirewall struct {
	Blocked []string
}

func (f *TestFirewall) Block(s string) error {
	f.Blocked = append(f.Blocked, s)

	return nil
}

func GeneratePackets() []gopacket.Packet {
	packets := []gopacket.Packet{}
	for i := 0; i < 3; i++ {
		port := 8080 + i
		buf := gopacket.NewSerializeBuffer()
		opts := gopacket.SerializeOptions{
			FixLengths:       true,
			ComputeChecksums: true,
		}
		ipLayer := layers.IPv4{
			SrcIP:    net.ParseIP("10.10.10.10"),
			DstIP:    net.ParseIP("127.0.0.1"),
			Protocol: layers.IPProtocolTCP,
			Options:  []layers.IPv4Option{},
			Length:   20,
		}
		tcpLayer := layers.TCP{
			SYN:     true,
			DstPort: layers.TCPPort(port),
			SrcPort: layers.TCPPort(32445),
			Options: []layers.TCPOption{},
		}

		if err := tcpLayer.SetNetworkLayerForChecksum(&ipLayer); err != nil {
			log.Println(err)
		}
		if err := gopacket.SerializeLayers(buf, opts, &ipLayer, &tcpLayer); err != nil {
			log.Println(err)
		}

		packetData := buf.Bytes()
		ip := gopacket.NewPacket(packetData, layers.LayerTypeIPv4, gopacket.NoCopy)
		packets = append(packets, ip)
	}

	return packets
}

func TestHandlePackets(t *testing.T) {
	viper.Set("block", true)

	packets := GeneratePackets()

	s, err := NewSource("lo")
	if err != nil {
		t.Fatal(err)
	}
	counter := prometheus.NewCounter(prometheus.CounterOpts{
		Subsystem: "connection_watcher",
		Name:      "total_new_tcp_connections",
		Help:      "total number of new http connections",
	})
	s.Counter = counter

	var f TestFirewall
	s.Blocker = &f
	for _, v := range packets {
		if err := s.handlePacket(v); err != nil {
			t.Fatal(err)
		}
	}

	if f.Blocked[0] != "10.10.10.10" {
		t.Error("expected 10.10.10.10 to be blocked")
	}
}

func TestGetWatcher(t *testing.T) {
	s, err := NewSource("lo")
	if err != nil {
		t.Fatal(err)
	}

	if s.Interface != "lo" {
		t.Errorf("interface is not correct")
	}

	if s.Address != "127.0.0.1" {
		t.Errorf("Address is not correct")
	}

}

func TestGetIP(t *testing.T) {
	tt := []struct {
		name   string
		want   gopacket.Layer
		actual string
		ok     bool
	}{
		{
			name: "good address",
			want: &layers.IPv4{
				SrcIP: net.IPv4(10, 10, 10, 10),
			},
			actual: "10.10.10.10",
			ok:     true,
		},
		{
			name:   "ipv6",
			want:   &layers.IPv6{},
			actual: "",
			ok:     false,
		},
	}

	for _, v := range tt {
		t.Run(v.name, func(t *testing.T) {
			got, ok := getIP(v.want)
			if got != v.actual {
				t.Errorf("Wanted %s, but got %s", v.actual, got)
			}

			if ok != v.ok {
				t.Errorf("Expected %v but got %v", v.ok, ok)
			}

		})

	}
}

func TestGetPort(t *testing.T) {

	tt := []struct {
		name   string
		want   gopacket.Layer
		actual string
		ok     bool
	}{
		{
			name: "tcp SYN with port",
			want: &layers.TCP{
				DstPort: layers.TCPPort(8080),
				SYN:     true,
			},
			actual: "8080",
			ok:     true,
		},
		{
			name: "tcp non-SYN with port",
			want: &layers.TCP{
				DstPort: layers.TCPPort(8080),
				SYN:     false,
			},
			actual: "",
			ok:     false,
		},
		{
			name:   "udp test",
			want:   &layers.UDP{},
			actual: "",
			ok:     false,
		},
	}

	for _, v := range tt {
		t.Run(v.name, func(t *testing.T) {
			got, ok := getPort(v.want)
			if got != v.actual {
				t.Errorf("Wanted %s, but got %s", v.actual, got)
			}

			if ok != v.ok {
				t.Errorf("Wanted %v, bot got %v", v.ok, ok)
			}
		})

	}
}
