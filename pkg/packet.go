package pkg

import (
	"fmt"
	"github.com/google/gopacket"
	"github.com/google/gopacket/layers"
	"github.com/google/gopacket/pcap"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/spf13/viper"
	"log"
	"net"
	"os/exec"
	"strings"
	"time"
)

type Watcher interface {
	Watch() error
}

type Blocker interface {
	Block(s string) error
}

type Firewall struct{}

func (f *Firewall) Block(address string) error {
	cmd := exec.Command("/usr/sbin/iptables", "-A", "INPUT", "-s", address, "-j", "DROP")
	err := cmd.Run()
	if err != nil {
		return err
	}

	return nil
}

// host holds host info for a packet
type host struct {
	// ports is a slice of destination ports this host has used
	ports map[string]time.Time

	// blocked is set if more than 3 ports are seen for this host in the past minute
	blocked bool

	// address is the IP address for this host
	address string
}

// Source holds info for our packet watcher.
type Source struct {
	// Interface is the network interface on which to watch packets.
	Interface string

	// Counter is the counter for the total number of connections.
	Counter prometheus.Counter

	// Blocked is the counter for total number of blocked hosts.
	Blocked prometheus.Counter

	// IPs is the map for IPs to ports.
	IPs map[string]*host

	// Address is the address of the network interface.
	Address string

	// Handle is the pcap handle on which to watch packets
	Handle *pcap.Handle

	// Blocker is the function to run to block an address
	Blocker Blocker
}

// getLocalAddress gets the address of the specified interface and stores
// it in the watcher's address field. Returns an error if one doesn't exist.
func (s *Source) getLocalAddress() error {
	var inter *net.Interface

	inter, err := net.InterfaceByName(s.Interface)
	if err != nil {
		return err
	}

	var addresses []net.Addr
	addresses, err = inter.Addrs()
	if err != nil {
		return err
	}

	var ipAddr net.IP

	for _, addr := range addresses {
		if ipAddr = addr.(*net.IPNet).IP.To4(); ipAddr != nil {
			break
		}
	}

	if ipAddr == nil {
		return fmt.Errorf("interface doesn't have an address")
	}

	s.Address = ipAddr.String()

	return nil
}

// NewSourcereturns a new source
func NewSource(eth string) (*Source, error) {

	ls := &Source{
		Interface: eth,
		IPs:       make(map[string]*host),
	}
	if err := ls.getLocalAddress(); err != nil {
		return nil, err
	}

	return ls, nil
}

// getIP takes a layer interface and returns the source IP if
// the layer is of type IPV4
func getIP(p gopacket.Layer) (string, bool) {
	// verify the packet is from IPv4
	ip, ok := p.(*layers.IPv4)
	if !ok {
		return "", false
	}

	return ip.SrcIP.String(), true
}

// getPort takes a Layer interface and return the destination port
// if the layer is of type TCP and SYN is true
func getPort(p gopacket.Layer) (string, bool) {
	// verify if the packet is TCP
	port, ok := p.(*layers.TCP)
	if !ok {
		fmt.Println("not port")
		return "", false
	}

	// verify if the packet is SYN
	if !port.SYN {
		return "", false
	}

	cleanPort := strings.SplitN(port.DstPort.String(), "(", 2)
	return cleanPort[0], true
}

// Watch watches the specified device for incoming packets.
func (s *Source) Watch() error {

	filter := fmt.Sprintf("tcp and not src host %s", s.Address)

	if err := s.Handle.SetBPFFilter(filter); err != nil {
		return err
	}

	packetSource := gopacket.NewPacketSource(s.Handle, s.Handle.LinkType())
	for packet := range packetSource.Packets() {
		if err := s.handlePacket(packet); err != nil {
			return err
		}
	}

	return nil
}

// handle Packet inspects a packetcoming from an external address and stores the addresses and ports
// requested in the source struct's host map. If more than 3 ports are requested from a single address
//it modifies the firewall to drop any incoming traffic from that address.
func (s *Source) handlePacket(packet gopacket.Packet) error {
	// get the IP from the packet
	ip, ok := getIP(packet.Layer(layers.LayerTypeIPv4))
	if !ok {
		return nil
	}

	// get the port from the packet
	port, ok := getPort(packet.Layer(layers.LayerTypeTCP))
	if !ok {
		return nil
	}

	h := s.updateMaps(ip, port)

	// if the block flag is set just report and return all connections
	if !viper.GetBool("block") {
		// increment counter because it's a new connection
		s.Counter.Inc()

		log.Printf("New Connection from host: %s:%s", ip, port)
		return nil
	}

	// if blocked is true, just continue because we've already blocked this port
	if h.blocked == true {
		return nil
	}

	h.checkTimestamps()

	// return because 3 or more ports haven't been seen within a minute.
	if h.blocked != true {
		return nil
	}

	// increment counter because it's a new connection
	s.Counter.Inc()

	log.Printf("Port scan detected %s -> %s on ports %v", ip, s.Address, prettyPorts(h.ports))
	if err := dropAddress(ip, s.Blocker); err != nil {
		return err
	}

	return nil
}

// updateMaps updates the map for IPs to ports in the watcher
func (s *Source) updateMaps(ip, port string) *host {
	h, ok := s.IPs[ip]
	if !ok {
		h = &host{
			address: ip,
		}
		s.IPs[ip] = h
	}

	if h.ports == nil {
		h.ports = make(map[string]time.Time)
	}

	h.ports[port] = time.Now()

	return h
}

// checkTimestamp compares the dates when each port was seen and if there have been 3 ports seen within a minute
// it sets the host blocked field to true
func (h *host) checkTimestamps() {
	now := time.Now()
	count := 0

	for _, v := range h.ports {
		if now.Sub(v) < time.Minute {
			count++
		}
	}

	if count >= 3 {
		h.blocked = true
	}
}

// dropAddress sets an iptables rule to drop the specified address
func dropAddress(address string, b Blocker) error {
	if err := b.Block(address); err != nil {
		return err
	}

	return nil
}

// prettyPorts just returns the ports in a string for printing
func prettyPorts(p map[string]time.Time) string {
	ports := make([]string, 0, len(p))
	for k := range p {
		ports = append(ports, k)
	}

	return strings.Join(ports, ",")
}
