# Blocker

This guide assumes you're using Fedora 35. It's also assuming you're running as root.

## Dependencies

### Docker

First run `dnf config-manager --add-repo https://download.docker.com/linux/fedora/docker-ce.repo`

Then run `dnf install docker-ce docker-ce-cli containerd.io`

Then run `systemctl enable --now docker`

### System 

Run `dnf install make golang nmap `

### Building

To build the project run `make linux`


### Testing

To run the tests just run `make test`


## Usage

First get the name of the interface you want to watch: `ip a`

To actually block addresses run:  `./blocker start -i <interface-name> -b`

If you only want to show new connections run: `./blocker start -i <interface-name>`

To send immediate requests to block run `nmap -Pn <address> -p 8080-80805`

To send requests that will pass after a minute run `curl <address>:8080; curl <address>:80801; sleep 61; curl <address>:8083`

Prometheus metrics are exported on port 9955

## Docker

### Easy Route

Pull from the container registry `docker pull registry.gitlab.com/hooksie1/blocker:latest`

### Harder Route

To build the container, run `make docker`

### Docker Usage

To run the container and block requests inside the container run `docker run --rm -p 9955:9955 -p 8000:8000 -p 8001:8001 -p 8002:8002 registry.gitlab.com/hooksie1/blocker:latest start -i eth0 -b`

If you want to run the container and report on connections to the host system run `docker run --rm --net=host -p 9955:9955 --cap-add=NET_ADMIN --cap-add=NET_RAW registry.gitlab.com/hooksie1/blocker start -i <interface>`