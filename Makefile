PROJECT_NAME := "blocker"
PKG := "gitlab.com/hooksie1/$(PROJECT_NAME)"
PKG_LIST := $(shell go list ${PKG}/... | grep -v /vendor/)
GO_FILES := $(shell find . -name '*.go' | grep -v /vendor/ | grep -v _test.go)
VERSION := $$(git describe --tags | cut -d '-' -f 1)

.PHONY: all build docker dep clean test coverage lint docker

all: build

lint: ## Lint the files
	golint -set_exit_status ./...

test: ## Run unittests
	go test -v ./...

coverage:
	go test -v -cover ./...
	go test ./... -coverprofile=cover.out && go tool cover -html=cover.out -o coverage.html

dep: ## Get the dependencies
	go get -u golang.org/x/lint/golint

build: linux

docker:
	docker build -t hooksie1/$(PROJECT_NAME) .

linux: dep
	GOOS=linux go build -o $(PROJECT_NAME)


clean: ## Remove previous build
	git clean -fd
	git clean -fx
	git reset --hard

help: ## Display this help screen
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
