module gitlab.com/hooksie1/blocker

go 1.16

require (
	github.com/google/gopacket v1.1.19
	github.com/prometheus/client_golang v1.4.0
	github.com/spf13/afero v1.8.0 // indirect
	github.com/spf13/cobra v1.3.0
	github.com/spf13/viper v1.10.1
	golang.org/x/net v0.0.0-20211015210444-4f30a5c0130f // indirect
	golang.org/x/sys v0.0.0-20220128215802-99c3d69c2c27 // indirect
	gopkg.in/ini.v1 v1.66.3 // indirect
)
