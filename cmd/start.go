/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>

*/
package cmd

import (
	"fmt"
	"github.com/google/gopacket/pcap"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/hooksie1/blocker/pkg"
	"log"
	"net/http"
)

// startCmd represents the start command
var startCmd = &cobra.Command{
	Use:   "start",
	Short: "starts the scanner and metrics exporter",
	Run:   start,
}

func init() {
	rootCmd.AddCommand(startCmd)

	startCmd.Flags().StringP("interface", "i", "lo", "Interface to watch")
	viper.BindPFlag("interface", startCmd.Flags().Lookup("interface"))
	startCmd.Flags().IntP("port", "p", 9955, "Port to expose metrics")
	viper.BindPFlag("port", startCmd.Flags().Lookup("port"))
	startCmd.Flags().BoolP("block", "b", false, "Block connections")
	viper.BindPFlag("block", startCmd.Flags().Lookup("block"))

}

// start starts the network watcher and the prometheus exporter
func start(cmd *cobra.Command, args []string) {

	// set up a prometheus counter to track the number of connections
	connCounter := prometheus.NewCounter(prometheus.CounterOpts{
		Subsystem: "connection_watcher",
		Name:      "total_new_tcp_connections",
		Help:      "total number of new http connections",
	})

	// get a new source to watch
	s, err := pkg.NewSource(viper.GetString("interface"))
	if err != nil {
		log.Fatal(err)
	}

	// open the interface and retrieve the *handle
	handle, err := pcap.OpenLive(s.Interface, 1600, true, pcap.BlockForever)
	if err != nil {
		log.Fatal(err)
	}

	// set the watcher's source to the live pcap
	s.Handle = handle

	// set the Prometheus counter in the watcher
	s.Counter = connCounter

	s.Blocker = &pkg.Firewall{}

	// start watcher in a separate goroutine.
	go func() {
		log.Printf("Watching for packets on %s", s.Address)
		if err := s.Watch(); err != nil {
			log.Println(err)
		}
	}()

	// register the connection counter with the exporter
	if err := prometheus.Register(connCounter); err != nil {
		// should check error type with errors.As and handle properly
		log.Println(err)
	}

	// set up the exporter handler
	http.Handle("/metrics", promhttp.Handler())
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		resp := fmt.Sprintf("<html>" +
			"<head><title>Jetstream Exporter</title></head>" +
			"<body>\n<h1>Jetstream Exporter</h1>" +
			"<p><a href='/metrics'>Metrics</a></p>" +
			"</body>\n</html>")
		fmt.Fprint(w, resp)
	})

	// get the port and start the webserver
	port := fmt.Sprintf(":%s", viper.GetString("port"))
	log.Fatal(http.ListenAndServe(port, nil))
}
