# Questions
## Level 1

### 1. How would you prove the code is correct?
Mostly through the tests. The unit tests generate packets in test tables with expected data and verify
the functions return the intended information. The one test that needs run manually is the actual blocking
of the connection. The unit tests have their own Blocker interface to verify the Blocker receives the correct data
but it's easier to test the connection is actually blocked manually by running the application and sending data.

### 2. How would you make this solution better?
I would use a BPF module or a firewall with hooks I could leverage instead of just calling cmd.Run(). Also figure out a 
way to modify the host firewall from inside the container. Modifying the firewall inside the container is easy since it's the same. 
I'm not sure how to modify the host firewall without something like gRPC or FIFO and use a client on the host.

### 3. Is it possible for this program to miss a connection?
Yes. From the requirements, this is only looking for TCP SYN packets. Any non TCP SYN packets could make it through 
the filter. 

### 4. If you weren't following these requirements, how would you solve the problem of logging every new connection?
Honestly, I would probably just use iptables for this. Setting any connections through iptables to be logged with
something similar to `iptables -I INPUT -p tcp --syn -j LOG`.

## Level 2

### 1. Why did you choose x to write the build automation?
Recently I started playing with goreleaser and I initially chose to use that for this project. However, needing CGO it made it
more difficult to build an image based off of their Docker container. So I went bck to make only because I've used it previously and it's popular.

### 2. Is there anything else you would test if you had more time?
Yeah just more tests in general I think. More negative cases. I'd also probably opt for some testing using a pcap file vs 
crafting the known packets to send manually, but would just have taken more time to implement.

### 3. What is the most important tool, script, or technique you have for solving problems in production? Explain why this tool/script/technique is the most important.
Even though I haven't used it much recently, I think I'd have to say Ansible is the most important tool I've had. It
allowed for ad-hoc commands to be run across many servers simultaneously. This saved me tons of time, and I was able to easily
compare output from multiple machines.

## Level 3

### 1. If you had to deploy this program to hundreds of servers, what would be your preferred method? Why?
I think it depends. If it's being deployed to ephemeral machines, I'd opt for something like Packer to build the template
and then be able to redeploy the machines. If that's not possible, I'd opt for some config-management system like Ansible.
Using either would be fairly trivial to set the service up as a systemd unit and just run as a service.

### 2. What is the hardest technical problem or outage you've had to solve in your career? Explain what made it so difficult?
I think the hardest would be figuring out a way to let users run Ansible to deploy WebLogic clusters without knowing how Ansible works. 
Since none of the users knew Ansible, and the centrally controlled Jenkins environment was locked down to only allow specific tools,
I had to create a YAML template that Ansible would read in one pipeline stage, then based on that template, generate playbooks,
group_vars, and an inventory. In the next stage, Ansible then used these playbooks, group_vars, and inventory to actually 
deploy the WebLogic cluster.

